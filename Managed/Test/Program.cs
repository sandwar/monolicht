using System;
using Monolicht;
using Monolicht.Scene;
using Monolicht.Video;
using Monolicht.Core;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Test
{
	class MainClass
	{
		public static void Main(string[] args) {
			using(var device = new IrrlichtDevice(DriverType.Software, new Dimension2D<uint>(640, 480), 16, false, false, false)) {

				device.WindowCaption = "Hello World! - Monolicht Demo";

				var driver = device.GetVideoDriver();
				var smgr = device.GetSceneManager();
				var guienv = device.GetGUIEnvironment();

				guienv.AddStaticText("Hello World! This is the Irrlicht Software renderer!", new Rect<int>(10, 10, 260, 22), true);

				AnimatedMesh mesh = smgr.GetMesh("../../../../../irrlicht/trunk/media/sydney.md2");
				AnimatedMeshSceneNode node = smgr.AddAnimatedMeshSceneNode(mesh);

				//node.SetMaterialFlag(EMF_LIGHTING, false);
				//node.SetMD2Animation(scene::EMAT_STAND);
				//node.SetMaterialTexture( 0, driver->getTexture("../../media/sydney.bmp") );

				smgr.AddCameraSceneNode(null, new Vector3D<float>(0, 30, -40), new Vector3D<float>(0, 5, 0));

				while(device.Run()) {
					driver.BeginScene(true, true, new SColor(255, 100, 101, 140));

					smgr.DrawAll();
					guienv.DrawAll();

					Thread.Sleep(10);
					driver.EndScene();
				}
			}
		}
	}
}
