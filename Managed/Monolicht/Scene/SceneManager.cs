using System;
using Monolicht.Internal;
using Monolicht.Core;

namespace Monolicht.Scene
{
	public class SceneManager : ReferenceCounted
	{
		public bool DrawAll() {
			return NativeStuff.irr_scene_ISceneManager_drawAll(this.ToNative());
		}

		public AnimatedMesh GetMesh(string path) {
			return this.Wrap(NativeStuff.irr_scene_ISceneManager_getMesh(this.ToNative(), path), () => new AnimatedMesh());
		}
		
		public AnimatedMeshSceneNode AddAnimatedMeshSceneNode(AnimatedMesh mesh, SceneNode parent = null, int id = -1, Vector3D<float> position = default(Vector3D<float>), Vector3D<float> rotation = default(Vector3D<float>)) {
			return AddAnimatedMeshSceneNode(mesh, parent, id, position, rotation, new Vector3D<float>(1, 1, 1));
		}
		public AnimatedMeshSceneNode AddAnimatedMeshSceneNode(AnimatedMesh mesh, SceneNode parent, int id, Vector3D<float> position, Vector3D<float> rotation, Vector3D<float> scale, bool alsoAddIfMeshPointerZero = false) {
			return this.Wrap(NativeStuff.irr_scene_ISceneManager_addAnimatedMeshSceneNode(this.ToNative(), mesh.ToNative(), parent.ToNative(), id, ref position, ref rotation, ref scale, alsoAddIfMeshPointerZero), () => new AnimatedMeshSceneNode());
		}

		public CameraSceneNode AddCameraSceneNode(SceneNode parent = null, Vector3D<float> position = default(Vector3D<float>)) {
			return AddCameraSceneNode(parent, position, new Vector3D<float>(0, 0, 100));
		}
		public CameraSceneNode AddCameraSceneNode(SceneNode parent, Vector3D<float> position, Vector3D<float> lookAt, int id = -1, bool makeActive = true) {
			return this.Wrap(NativeStuff.irr_scene_ISceneManager_addCameraSceneNode(this.ToNative(), parent.ToNative(), ref position, ref lookAt, id, makeActive), () => new CameraSceneNode());
		}
	}
}

