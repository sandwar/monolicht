using System;
using Monolicht.GUI;
using Monolicht.Core;
using Monolicht.Internal;

namespace Monolicht.GUI
{
	public class GUIEnvironment : INativeObject
	{
		public GUIStaticText AddStaticText(string text, Rect<int> rect, bool border=false, bool wordWrap=true, GUIElement parent=null, int id=-1, bool fillBackground=false) {
			//using(var nativeText = new NativeString(text))
				return this.Wrap(NativeStuff.irr_gui_IGUIEnvironment_addStaticText(this.ToNative(), text, ref rect, border, wordWrap, parent.ToNative(), id, fillBackground), () => new GUIStaticText());
		}

		public bool DrawAll() {
			return NativeStuff.irr_gui_IGUIEnvironment_drawAll(this.ToNative());
		}

		IntPtr INativeObject.NativePointer { get; set; }
	}
}

