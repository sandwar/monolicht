using System;
using System.Runtime.CompilerServices;
using Monolicht.Core;
using Monolicht.GUI;
using Monolicht.Internal;
using Monolicht.Scene;
using Monolicht.Video;

namespace Monolicht
{
	public class IrrlichtDevice : INativeObject, IDisposable
	{
		public string WindowCaption {
			set { NativeStuff.irr_IrrlichtDevice_setWindowCaption(((INativeObject)this).NativePointer, value); }
		}

		public IrrlichtDevice(DriverType deviceType, Dimension2D<UInt32> windowSize, UInt32 bits, Boolean fullscreen, Boolean stencilBuffer, Boolean vSync) {
			NativeStuff.initialize();
			((INativeObject)this).NativePointer = hideFromJit(deviceType, windowSize, bits, fullscreen, stencilBuffer, vSync);
			if(((INativeObject)this).NativePointer == IntPtr.Zero)
				throw new NotSupportedException("Failed to start Irrlicht");
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IntPtr hideFromJit(DriverType deviceType, Dimension2D<UInt32> windowSize, UInt32 bits, Boolean fullscreen, Boolean stencilBuffer, Boolean vSync) {
			return NativeStuff.irr_createDevice(deviceType, windowSize, bits, fullscreen, stencilBuffer, vSync, IntPtr.Zero);
		}

		public VideoDriver GetVideoDriver() {
			return this.Wrap(NativeStuff.irr_IrrlichtDevice_getVideoDriver(this.ToNative()), () => new VideoDriver());
		}

		public SceneManager GetSceneManager() {
			return this.Wrap(NativeStuff.irr_IrrlichtDevice_getSceneManager(this.ToNative()), () => new SceneManager());
		}

		public GUIEnvironment GetGUIEnvironment() {
			return this.Wrap(NativeStuff.irr_IrrlichtDevice_getGUIEnvironment(this.ToNative()), () => new GUIEnvironment());
		}

		public bool Run() {
			return NativeStuff.irr_IrrlicthDevice_run(this.ToNative());
		}

		public void Dispose() {
			NativeStuff.irr_IrrlichtDevice_drop(this.ToNative());
		}

		IntPtr INativeObject.NativePointer { get; set; }
	}
}

