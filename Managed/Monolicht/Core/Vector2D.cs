using System;
using System.Runtime.InteropServices;

namespace Monolicht.Core
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Vector2D<T>
	{
		public T X;
		public T Y;

		public Vector2D(T x, T y) {
			this.X = x;
			this.Y = y;
		}
	}
}

