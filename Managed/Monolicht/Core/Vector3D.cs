using System;
using System.Runtime.InteropServices;

namespace Monolicht.Core
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Vector3D<T>
	{
		public T X;
		public T Y;
		public T Z;

		public Vector3D(T x, T y, T z) {
			this.X = x;
			this.Y = y;
			this.Z = z;
		}
	}
}

