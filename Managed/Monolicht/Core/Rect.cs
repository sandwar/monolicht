using System;
using System.Runtime.InteropServices;

namespace Monolicht.Core
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Rect<T>
	{
		public Vector2D<T> UpperLeftCorner;
		public Vector2D<T> LowerRightCorner;

		public Rect(T x, T y, T x2, T y2) {
			this.UpperLeftCorner = new Vector2D<T>(x, y);
			this.LowerRightCorner = new Vector2D<T>(x2, y2);
		}
	}
}

