using System;
using System.Runtime.InteropServices;

namespace Monolicht.Core
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Dimension2D<T>
	{
		public T Width;
		public T Height;

		public Dimension2D(T width, T height) {
			this.Width = width;
			this.Height = height;
		}
	}
}

