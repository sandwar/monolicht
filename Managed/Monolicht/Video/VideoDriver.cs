using System;
using Monolicht.Internal;
using Monolicht.Core;

namespace Monolicht.Video
{
	public class VideoDriver : ReferenceCounted
	{
		public bool BeginScene(bool backBuffer=true, bool zBuffer=true) {
			return BeginScene(backBuffer, zBuffer, new SColor(255, 0, 0, 0));
		}

		public unsafe bool BeginScene(bool backBuffer, bool zBuffer, SColor color, SExposedVideoData videoData=new SExposedVideoData(), Rect<int> sourceRect=default(Rect<int>)) {
			// (sourceRect == default(Rect<int>)) ? IntPtr.Zero : new IntPtr(&sourceRect)
			return NativeStuff.irr_video_IVideoDriver_beginScene(this.ToNative(), backBuffer, zBuffer, color, new IntPtr(&videoData), IntPtr.Zero);
		}

		public bool EndScene() {
			return NativeStuff.irr_video_IVideoDriver_endScene(this.ToNative());
		}
	}
}

