using System;

namespace Monolicht.Video
{
	public enum DriverType : int
	{
		Null,
		Software,
		BurningsVideo,
		Direct3D8,
		Direct3D9,
		OpenGL,
		Count
	}
}

