using System;
using System.Runtime.InteropServices;

namespace Monolicht.Video
{
	[StructLayout(LayoutKind.Sequential)]
	public struct SColor
	{
		public uint Color;

		public SColor(uint a, uint r, uint g, uint b) {
			this.Color = ((a & 0xff)<<24) | ((r & 0xff)<<16) | ((g & 0xff)<<8) | (b & 0xff);
		}
	}
}

