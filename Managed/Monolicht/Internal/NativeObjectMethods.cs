using System;

namespace Monolicht.Internal
{
	internal static class NativeObjectMethods {
		
		public static IntPtr ToNative(this INativeObject nativeObject) {
			if(nativeObject == null)
				return IntPtr.Zero;

			return nativeObject.NativePointer;
		}

		public static T Wrap<T>(this INativeObject nativeObject, IntPtr nativePointer, Func<T> create) where T : INativeObject {
			return NativeCache.Global.Get<T>(nativePointer, create);
		}
	}
}
