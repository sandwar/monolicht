using System;
using System.Collections.Generic;

namespace Monolicht.Internal
{
	internal class NativeCache
	{
		public static readonly NativeCache Global = new NativeCache();
		
		// TODO: make references weak
		private Dictionary<IntPtr, object> cache = new Dictionary<IntPtr, object>();

		public T Get<T>(IntPtr nativePointer, T wrapped) where T : INativeObject {
			wrapped.NativePointer = nativePointer;
			cache[nativePointer] = wrapped;
			return wrapped;
		}

		public T Get<T>(IntPtr nativePointer, Func<T> create) where T : INativeObject {
			object val;
			if(cache.TryGetValue(nativePointer, out val))
			   return (T)val;

			return Get<T>(nativePointer, create());
		}
	}
}

