using System;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using Monolicht.Video;
using Monolicht.Core;
using System.Diagnostics;
using System.IO;

namespace Monolicht.Internal
{
	internal static class NativeStuff
	{
#if DEBUG
		const string libMonolicht = "../../../../Native/bin/Release/libMonolicht.so";
#else
		const string libMonolicht = "libMonolicht";
#endif

		static bool initialized = false;

		[DllImport(libMonolicht)]
		static extern void init();

		internal static void initialize() {
			if(!initialized) {
				initialized = true;
				init();
			}
		}

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_createDevice(DriverType deviceType, Dimension2D<UInt32> windowSize, UInt32 bits, Boolean fullscreen, Boolean stencilBuffer, Boolean vSync, IntPtr receiver);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern string irr_IReferenceCounted_getDebugName(IntPtr referenceCounted);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern void irr_IReferenceCounted_setDebugName(IntPtr referenceCounted, string name);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern void irr_IrrlichtDevice_drop(IntPtr device);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern void irr_IrrlichtDevice_setWindowCaption(IntPtr device, string caption);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_IrrlichtDevice_getVideoDriver(IntPtr device);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_IrrlichtDevice_getSceneManager(IntPtr device);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_IrrlichtDevice_getGUIEnvironment(IntPtr device);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_gui_IGUIEnvironment_addStaticText(IntPtr gui, string text, ref Rect<Int32> rect, bool border, bool wordWrap, IntPtr parent, int id, bool fillBackground);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern bool irr_IrrlicthDevice_run(IntPtr device);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern bool irr_video_IVideoDriver_beginScene(IntPtr driver, bool backBuffer, bool zBuffer, SColor color, IntPtr videoData, IntPtr sourceRect);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern bool irr_video_IVideoDriver_endScene(IntPtr driver);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern bool irr_scene_ISceneManager_drawAll(IntPtr manager);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_scene_ISceneManager_getMesh(IntPtr manager, string path);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_scene_ISceneManager_addAnimatedMeshSceneNode(IntPtr manager, IntPtr mesh, IntPtr parent, Int32 id, ref Vector3D<Single> position, ref Vector3D<Single> rotation, ref Vector3D<Single> scale, bool alsoAddIfMeshPointerZero);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern IntPtr irr_scene_ISceneManager_addCameraSceneNode(IntPtr manager, IntPtr parent, ref Vector3D<Single> position, ref Vector3D<Single> lookAt, Int32 id, bool makeActive);

		[DllImport(libMonolicht), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		public static extern bool irr_gui_IGUIEnvironment_drawAll(IntPtr env);
	}
}

