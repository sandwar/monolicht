using System;

namespace Monolicht.Internal
{
	internal interface INativeObject
	{
		IntPtr NativePointer { get; set; }
	}
}

