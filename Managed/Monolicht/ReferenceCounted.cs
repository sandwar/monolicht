using System;
using Monolicht.Internal;
using System.Diagnostics;

namespace Monolicht
{
	public class ReferenceCounted : INativeObject
	{
		IntPtr INativeObject.NativePointer { get; set; }

		public int ReferenceCount {
			get { return 0; }
		}

#if !DEBUG
		[Obsolete("This method should only be used in Debug mode.")]
#endif
		public string DebugName {
			get {
#if DEBUG
				return NativeStuff.irr_IReferenceCounted_getDebugName(this.ToNative());
#else
				return string.Empty;
#endif
			}
			/*protected*/ set {
#if DEBUG
				NativeStuff.irr_IReferenceCounted_setDebugName(this.ToNative(), value);
#endif
			}
		}

		public void Grab() {
			//return NativeStuff.irr_IReferenceCounted_grab(this.ToNative());
		}

		public void Drop() {
			//return NativeStuff.irr_IReferenceCounted_drop(this.ToNative());
		}
	}
}

