// CIL STUFF
#include <vector>
#include <utility>

std::vector<std::pair<const char*, void*> > __cilExportedFunctionsVector__;

// CIL_EXPORT(function_name, return_value, arg1_type arg1_value, arg2_type arg2_value, ..., argn_type argn_value) { function_body }
#define CIL_EXPORT(FUNC, RETVAL, ...) \
/*extern "C"*/ RETVAL FUNC (__VA_ARGS__); \
class FUNC##__staticInit__ { public: FUNC##__staticInit__() { \
__cilExportedFunctionsVector__.push_back( std::make_pair(MONO_MANAGED_CLASS "::" #FUNC, (void*)FUNC) ); \
}; }; FUNC##__staticInit__ FUNC##__staticInit__instance__; \
RETVAL FUNC (__VA_ARGS__)

#include <glib.h>
#include <mono/metadata/object.h>
#include <mono/metadata/metadata.h>
#include <mono/metadata/class.h>
#include <mono/metadata/appdomain.h>
#include <mono/metadata/loader.h>

extern "C" void init()
{
	for (unsigned int i=0; i<__cilExportedFunctionsVector__.size(); i++)
		mono_add_internal_call((__cilExportedFunctionsVector__[i]).first, (__cilExportedFunctionsVector__[i]).second);
}

wchar_t* mono_string_to_wide(MonoString* mono) {
	if(!mono)
		return 0;

	int length = mono_string_length(mono);
	wchar_t* native = new wchar_t[length+1];

	gunichar2* pointer = mono_string_chars(mono);
	for(int i = 0; i < length; i++)
		native[i] = pointer[i];

	native[length] = 0;

	return native;
}

char* mono_string_to_chars(MonoString* mono) {
	if(!mono)
		return 0;

	int length = mono_string_length(mono);
	char* native = new char[length+1];

	gunichar2* pointer = mono_string_chars(mono);
	for(int i = 0; i < length; i++)
		native[i] = pointer[i];

	native[length] = 0;

	return native;
}

MonoString* chars_to_mono_string(const char* chars) {
	if(!chars)
		return 0;

	return mono_string_new(mono_domain_get(), chars);
}

#include <irrlicht.h>

#define MONO_MANAGED_CLASS "Monolicht.Internal.NativeStuff"

CIL_EXPORT(irr_createDevice, irr::IrrlichtDevice*, irr::video::E_DRIVER_TYPE deviceType, irr::core::dimension2d<irr::u32> windowSize, irr::u32 bits, bool fullscreen, bool stencilBuffer, bool vSync, irr::IEventReceiver* receiver) {
	return irr::createDevice(deviceType, windowSize, bits, fullscreen, stencilBuffer, vSync, receiver);
}

CIL_EXPORT(irr_IReferenceCounted_getDebugName, MonoString*, irr::IReferenceCounted* refCount) {
	return chars_to_mono_string(refCount->getDebugName());
}

template<typename T> struct Unprotect : public T {
	typedef void (T::*FooPtr)(const char*);
	static FooPtr GetFooPtr() { return &Unprotect::setDebugName; }
};
template<typename T> class Bar {
public:
	static void bar(T* self, const char* c) {
		(self->*Unprotect<irr::IReferenceCounted>::GetFooPtr())(c);
	}
};

CIL_EXPORT(irr_IReferenceCounted_setDebugName, void, irr::IReferenceCounted* refCount, MonoString* name) {
#if !DEBUG
	char* name2 = mono_string_to_chars(name);

	Bar<irr::IReferenceCounted>::bar(refCount, name2);

	//delete name2;   // TODO: if we clean getDebugName will stop working, but if we don't it's a memory leak
#endif
}

CIL_EXPORT(irr_IrrlichtDevice_drop, void, irr::IrrlichtDevice* device) {
	device->drop();
}

CIL_EXPORT(irr_IrrlichtDevice_setWindowCaption, void, irr::IrrlichtDevice* device, MonoString* caption) {
	wchar_t* caption2 = mono_string_to_wide(caption);
	device->setWindowCaption(caption2);

	delete caption2;
}

CIL_EXPORT(irr_IrrlichtDevice_getVideoDriver, irr::video::IVideoDriver*, irr::IrrlichtDevice* device) {
	return device->getVideoDriver();
}

CIL_EXPORT(irr_IrrlichtDevice_getSceneManager, irr::scene::ISceneManager*, irr::IrrlichtDevice* device) {
	return device->getSceneManager();
}

CIL_EXPORT(irr_IrrlichtDevice_getGUIEnvironment, irr::gui::IGUIEnvironment*, irr::IrrlichtDevice* device) {
	return device->getGUIEnvironment();
}

CIL_EXPORT(irr_gui_IGUIEnvironment_addStaticText, irr::gui::IGUIStaticText*, irr::gui::IGUIEnvironment* gui, MonoString* text, irr::core::rect<irr::s32>& rect, bool border, bool wordWrap, irr::gui::IGUIElement* parent, irr::s32 id, bool fillBackground) {
	wchar_t* text2 = mono_string_to_wide(text);

	irr::gui::IGUIStaticText* retval = gui->addStaticText(text2, rect, border, wordWrap, parent, id, fillBackground);

	delete text2;

	return retval;
}

CIL_EXPORT(irr_IrrlicthDevice_run, bool, irr::IrrlichtDevice* device) {
	return device->run();
}

CIL_EXPORT(irr_video_IVideoDriver_beginScene, bool, irr::video::IVideoDriver* driver, bool backBuffer, bool zBuffer, irr::video::SColor color) {
	return driver->beginScene(backBuffer, zBuffer, color);
}

CIL_EXPORT(irr_video_IVideoDriver_endScene, bool, irr::video::IVideoDriver* driver) {
	return driver->endScene();
}

CIL_EXPORT(irr_scene_ISceneManager_drawAll, void, irr::scene::ISceneManager* manager) {
	manager->drawAll();
}

CIL_EXPORT(irr_scene_ISceneManager_getMesh, irr::scene::IAnimatedMesh*, irr::scene::ISceneManager* manager, MonoString* path) {
	char* path2 = mono_string_to_chars(path);
	irr::scene::IAnimatedMesh* retval = manager->getMesh(path2);
	delete path2;
	return retval;
}

CIL_EXPORT(irr_scene_ISceneManager_addAnimatedMeshSceneNode, irr::scene::IAnimatedMeshSceneNode*, irr::scene::ISceneManager* manager, irr::scene::IAnimatedMesh* mesh, irr::scene::ISceneNode* parent, int id, irr::core::vector3df position, irr::core::vector3df rotation, irr::core::vector3df scale, bool alsoAddIfMeshPointerZero) {
	return manager->addAnimatedMeshSceneNode(mesh, parent, id, position, rotation, scale, alsoAddIfMeshPointerZero);
}

CIL_EXPORT(irr_scene_ISceneManager_addCameraSceneNode, irr::scene::ICameraSceneNode*, irr::scene::ISceneManager* manager, irr::scene::ISceneNode* parent, irr::core::vector3df position, irr::core::vector3df lookAt, int id, bool makeActive) {
	return manager->addCameraSceneNode(parent, position, lookAt, id, makeActive);
}

CIL_EXPORT(irr_gui_IGUIEnvironment_drawAll, void, irr::gui::IGUIEnvironment* env) {
	env->drawAll();
}
